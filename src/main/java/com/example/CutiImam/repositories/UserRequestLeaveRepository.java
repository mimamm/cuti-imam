package com.example.CutiImam.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.CutiImam.models.UserRequestLeave;

@Repository
public interface UserRequestLeaveRepository extends JpaRepository<UserRequestLeave, Long> {
	
}