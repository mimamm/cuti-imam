package com.example.CutiImam.dtos;

import java.util.Date;

public class UsersDto {
	private long userId;
	private PositionDto position;
	private String name;
	private int leaveTotal;
	private Date createdAt;
	private String createdBy;
	private Date updatedAt;
	private String updatedBy;
	
	public UsersDto() {
		// TODO Auto-generated constructor stub
	}

	public UsersDto(long userId, PositionDto position, String name, int leaveTotal, Date createdAt, String createdBy,
			Date updatedAt, String updatedBy) {
		super();
		this.userId = userId;
		this.position = position;
		this.name = name;
		this.leaveTotal = leaveTotal;
		this.createdAt = createdAt;
		this.createdBy = createdBy;
		this.updatedAt = updatedAt;
		this.updatedBy = updatedBy;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public PositionDto getPosition() {
		return position;
	}

	public void setPosition(PositionDto position) {
		this.position = position;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getLeaveTotal() {
		return leaveTotal;
	}

	public void setLeaveTotal(int leaveTotal) {
		this.leaveTotal = leaveTotal;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	
}