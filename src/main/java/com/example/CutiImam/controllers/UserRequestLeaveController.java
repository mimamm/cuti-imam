package com.example.CutiImam.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.CutiImam.dtos.UserRequestLeaveDto;
import com.example.CutiImam.models.UserRequestLeave;
import com.example.CutiImam.models.Users;
import com.example.CutiImam.repositories.UserRequestLeaveRepository;
import com.example.CutiImam.repositories.UsersRepository;
import com.example.CutiImam.exceptions.ResourceNotFoundException;
import com.example.CutiImam.exceptions.UserLeaveRequestException;

@RestController
@RequestMapping("/api")
public class UserRequestLeaveController {
	
	@Autowired
	UserRequestLeaveRepository userRequestLeaveRepository;
	
	@Autowired
	UsersRepository usersRepository;
	
	ModelMapper modelMapper = new ModelMapper();
	
//	API Request Leave (No.2)
	@PostMapping("/requestleave")
	public Map<String, Object> requestLeave(@RequestBody UserRequestLeaveDto body) {
		Map<String, Object> output = new HashMap<String, Object>();
		
		UserRequestLeave userRequestLeaveEntity = modelMapper.map(body, UserRequestLeave.class);
		Date today = new Date();
		
		userRequestLeaveEntity.setLeaveTotalLeft(getUserLeaveTotalLeft(userRequestLeaveEntity.getUsers()));
		
		validasiJatahCuti(userRequestLeaveEntity.getLeaveTotalLeft(), userRequestLeaveEntity.getLeaveDateFrom(), userRequestLeaveEntity.getLeaveDateTo());

		userRequestLeaveEntity.setRequestDate(today);
		userRequestLeaveEntity.setStatus("Waiting");
		
		userRequestLeaveRepository.save(userRequestLeaveEntity);
		
		output.put("Message", "Permohonan Anda sedang diproses");
		
		return output;
	}
	
//	API List Request Leave (No.3)
	@GetMapping("/listRequestLeave/{userId}/{totalDataPerPage}/{choosenPage}")
	public Map<String, Object> listRequestLeave(@PathVariable(value = "userId") Long userId, @PathVariable(value = "totalDataPerPage") int totalDataPerPage, @PathVariable(value = "choosenPage") int choosenPage) {
		Map<String, Object> output = new HashMap<String, Object>();

		Pageable pageWithElement = PageRequest.of(choosenPage, totalDataPerPage);
		
		List<UserRequestLeave> listEntity = userRequestLeaveRepository.findAll();
		List<UserRequestLeaveDto> listDto = new ArrayList<UserRequestLeaveDto>();
		
		for (UserRequestLeave userRequestLeave : listEntity) {
			if (userRequestLeave.getUsers().getUserId() == userId) {
				UserRequestLeaveDto dto = modelMapper.map(userRequestLeave, UserRequestLeaveDto.class);
				listDto.add(dto);
			}
		}
		
		int start = (int) pageWithElement.getOffset();
		int end = (int) (start + pageWithElement.getPageSize()) > listDto.size() ? listDto.size() : (start + pageWithElement.getPageSize());
		int totalRows = listDto.size();
		
		Page<UserRequestLeaveDto> pagedDto = new PageImpl<UserRequestLeaveDto>(listDto .subList(start, end), pageWithElement, totalRows); 
		List<UserRequestLeaveDto> outputList = pagedDto.getContent();
		
		output.put("Items", outputList);
		output.put("Total Items", pagedDto.getTotalElements());
		
		return output;
	}
	
//	API List Request Leave by status (No.5)
	@GetMapping("/listRequestLeaveByStatus/{status}/{totalDataPerPage}/{choosenPage}")
	public Map<String, Object> listRequestLeaveByStatus(@PathVariable(value = "status") String status, @PathVariable(value = "totalDataPerPage") int totalDataPerPage, @PathVariable(value = "choosenPage") int choosenPage) {
		Map<String, Object> output = new HashMap<String, Object>();

		Pageable pageWithElement = PageRequest.of(choosenPage, totalDataPerPage);
		
		List<UserRequestLeave> listEntity = userRequestLeaveRepository.findAll();
		List<UserRequestLeaveDto> listDto = new ArrayList<UserRequestLeaveDto>();
		
		for (UserRequestLeave userRequestLeave : listEntity) {
			if (userRequestLeave.getStatus().equalsIgnoreCase(status)) {
				UserRequestLeaveDto dto = modelMapper.map(userRequestLeave, UserRequestLeaveDto.class);
				listDto.add(dto);
			}
		}
		
		int start = (int) pageWithElement.getOffset();
		int end = (int) (start + pageWithElement.getPageSize()) > listDto.size() ? listDto.size() : (start + pageWithElement.getPageSize());
		int totalRows = listDto.size();
		
		Page<UserRequestLeaveDto> pagedDto = new PageImpl<UserRequestLeaveDto>(listDto .subList(start, end), pageWithElement, totalRows); 
		List<UserRequestLeaveDto> outputList = pagedDto.getContent();
		
		output.put("Items", outputList);
		output.put("Total Items", pagedDto.getTotalElements());
		
		return output;
	}
	
//	=======================================================================================================
	
//	Get User Leave Total Left
	public int getUserLeaveTotalLeft(Users user) {
		Users userWithLeaveTotalLeft = usersRepository.findById(user.getUserId()).orElseThrow(() -> new ResourceNotFoundException("User", "ID", user.getUserId()));
		int leaveTotalLeft = userWithLeaveTotalLeft.getLeaveTotal();
		
		return leaveTotalLeft;		
	}
	
//	Validasi Jatah Cuti
	public void validasiJatahCuti(int leaveTotalLeft, Date dateFrom, Date dateTo) {
		long diff = dateTo.getTime() - dateFrom.getTime();
		long daysBetween = (diff / (24 * 60 * 60 * 1000)) + 1;
		Date today = new Date();
		
		if (leaveTotalLeft == 0) {
			throw new UserLeaveRequestException("Mohon maaf, jatah cuti Anda telah habis");
		} else if (dateFrom.before(today) || dateTo.before(today)) {
			throw new UserLeaveRequestException("Tanggal yang Anda ajukan telah lampau, silahkan ganti tanggal pengajuan cuti Anda");
		} else if (diff < 0) {
			throw new UserLeaveRequestException("Maaf tanggal yang Anda ajukan tidak valid");
		} else if (daysBetween > leaveTotalLeft) {
			throw new UserLeaveRequestException("Mohon maaf, jatah cuti Anda tidak cukup untuk digunakan dari tanggal " + dateFrom +
					" sampai " + dateTo + " (" + daysBetween + " hari). Jatah cuti Anda yang tersisa adalah " + leaveTotalLeft + " hari.");
		}
	}
}