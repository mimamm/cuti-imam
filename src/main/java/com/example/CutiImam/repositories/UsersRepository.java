package com.example.CutiImam.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.CutiImam.models.Users;

@Repository
public interface UsersRepository extends JpaRepository<Users, Long> {

}