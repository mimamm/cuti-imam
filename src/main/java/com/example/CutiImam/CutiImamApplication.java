package com.example.CutiImam;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class CutiImamApplication {

	public static void main(String[] args) {
		SpringApplication.run(CutiImamApplication.class, args);
	}

	@Bean
	AuditorAware<String> auditorProvider() {
	    return new AuditorAwareImpl();
	}
}
