package com.example.CutiImam.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.CutiImam.dtos.UsersDto;
import com.example.CutiImam.exceptions.ResourceNotFoundException;
import com.example.CutiImam.models.Position;
import com.example.CutiImam.models.PositionLeave;
import com.example.CutiImam.models.Users;
import com.example.CutiImam.repositories.PositionRepository;
import com.example.CutiImam.repositories.UsersRepository;

@RestController
@RequestMapping("/api/users")
public class UsersController {

	@Autowired
	UsersRepository usersRepository;
	
	@Autowired
	PositionRepository positionRepository;
	
	ModelMapper modelMapper = new ModelMapper();

//  Create a new User
    @PostMapping("/create")
    public Map<String, Object> createUser(@RequestBody UsersDto body) {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	Users userEntity = modelMapper.map(body, Users.class);
    	
    	userEntity.setLeaveTotal(getTotalLeave(userEntity.getPosition().getPositionId()));
    	
    	usersRepository.save(userEntity);
    	
    	body.setUserId(userEntity.getUserId());
    	result.put("Message", "Create User success");
    	result.put("Result", body);
    	
    	return result;
    }
    
//  Get Total Leave (Jatah Cuti)
    public int getTotalLeave(Long positionId) {
    	Position position = positionRepository.findById(positionId).orElseThrow(() -> new ResourceNotFoundException("Position", "ID", positionId));
    	
    	Set<PositionLeave> listPositionLeave = position.getPositionLeaves();
    	int totalLeave = 0;
    	
    	for (PositionLeave positionLeave : listPositionLeave) {
			if (positionLeave.getPosition().getPositionId() == positionId) {
				totalLeave = positionLeave.getLeaveTotal();
			}
		}
    	return totalLeave;
    }

//  Update a User
    @PutMapping("/update")
    public Map<String, Object> updateUser(@RequestParam("userId") Long id, @RequestBody UsersDto body) {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	Users userEntity = usersRepository.findById(id).orElseThrow(()-> new ResourceNotFoundException("User", "ID", id));
    	Date createdAt = userEntity.getCreatedAt();
    	String createdBy = userEntity.getCreatedBy();
    	
    	userEntity = modelMapper.map(body, Users.class);
    	userEntity.setUserId(id);
    	userEntity.setCreatedAt(createdAt);
    	userEntity.setCreatedBy(createdBy);
    	userEntity.setLeaveTotal(getTotalLeave(body.getPosition().getPositionId()));
    	
    	usersRepository.save(userEntity);
    	
    	body.setUserId(userEntity.getUserId());;
    	result.put("Message", "Update User success");
    	result.put("Result", body);
    	
    	return result;
    }

//  Delete a User
    @DeleteMapping("/delete")
    public Map<String, Object> deleteUser(@RequestParam("userId") Long id) {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	Users userEntity = usersRepository.findById(id).orElseThrow(()-> new ResourceNotFoundException("User", "ID", id));
    	
    	UsersDto userDto = modelMapper.map(userEntity, UsersDto.class);
    	
    	usersRepository.delete(userEntity);
    	
    	result.put("Message", "Delete User success");
    	result.put("Deleted Data", userDto);
    	
    	return result;
    }

//  Get a single Users by ID
    @GetMapping("/get")
    public Map<String, Object> getUsersById(@RequestParam("userId") Long id) {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	Users userEntity = usersRepository.findById(id).orElseThrow(()-> new ResourceNotFoundException("User", "ID", id));
    	
    	UsersDto userDto = modelMapper.map(userEntity, UsersDto.class);
    	
    	result.put("Result", userDto);
    	
    	return result;
    }
    
//  Get all Users
    @GetMapping("/getAll")
    public Map<String, Object> getAllUsers() {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	List<Users> listEntity = usersRepository.findAll();
    	List<UsersDto> listDto = new ArrayList<UsersDto>();

    	for (Users userEntity : listEntity) {
    		UsersDto userDto = modelMapper.map(userEntity, UsersDto.class);
    		
    		listDto.add(userDto);
    	}
    	
    	result.put("Result", listDto);
    	
    	return result;
    }
}