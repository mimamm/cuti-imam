package com.example.CutiImam.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.CutiImam.dtos.PositionDto;
import com.example.CutiImam.exceptions.ResourceNotFoundException;
import com.example.CutiImam.models.Position;
import com.example.CutiImam.repositories.PositionRepository;

@RestController
@RequestMapping("/api/position")
public class PositionController {

	@Autowired
	PositionRepository positionRepository;
	
	ModelMapper modelMapper = new ModelMapper();

//  Create a new position
    @PostMapping("/create")
    public Map<String, Object> createposition(@RequestBody PositionDto body) {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	Position positionEntity = modelMapper.map(body, Position.class);
    	
    	positionRepository.save(positionEntity);
    	
    	body.setPositionId(positionEntity.getPositionId());
    	result.put("Message", "Create position success");
    	result.put("Result", body);
    	
    	return result;
    }

//  Update a position
    @PutMapping("/update")
    public Map<String, Object> updateposition(@RequestParam("positionId") Long id, @RequestBody PositionDto body) {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	Position positionEntity = positionRepository.findById(id).orElseThrow(()-> new ResourceNotFoundException("position", "ID", id));
    	Date createdAt = positionEntity.getCreatedAt();
    	String createdBy = positionEntity.getCreatedBy();
    	
    	positionEntity = modelMapper.map(body, Position.class);
    	positionEntity.setPositionId(id);
    	positionEntity.setCreatedAt(createdAt);
    	positionEntity.setCreatedBy(createdBy);
    	
    	positionRepository.save(positionEntity);
    	
    	body.setPositionId(positionEntity.getPositionId());;
    	result.put("Message", "Update Position success");
    	result.put("Result", body);
    	
    	return result;
    }

//  Delete a position
    @DeleteMapping("/delete")
    public Map<String, Object> deleteposition(@RequestParam("positionId") Long id) {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	Position positionEntity = positionRepository.findById(id).orElseThrow(()-> new ResourceNotFoundException("Position", "ID", id));
    	
    	PositionDto PositionDto = modelMapper.map(positionEntity, PositionDto.class);
    	
    	positionRepository.delete(positionEntity);
    	
    	result.put("Message", "Delete Position success");
    	result.put("Deleted Data", PositionDto);
    	
    	return result;
    }

//  Get a single Position by ID
    @GetMapping("/get")
    public Map<String, Object> getPositionById(@RequestParam("positionId") Long id) {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	Position positionEntity = positionRepository.findById(id).orElseThrow(()-> new ResourceNotFoundException("Position", "ID", id));
    	
    	PositionDto positionDto = modelMapper.map(positionEntity, PositionDto.class);
    	
    	result.put("Result", positionDto);
    	
    	return result;
    }
    
//  Get all Position
    @GetMapping("/getAll")
    public Map<String, Object> getAllPosition() {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	List<Position> listEntity = positionRepository.findAll();
    	List<PositionDto> listDto = new ArrayList<PositionDto>();

    	for (Position positionEntity : listEntity) {
    		PositionDto positionDto = modelMapper.map(positionEntity, PositionDto.class);
    		
    		listDto.add(positionDto);
    	}
    	
    	result.put("Result", listDto);
    	
    	return result;
    }
}