/*
 Navicat Premium Data Transfer

 Source Server         : PostgresConnection
 Source Server Type    : PostgreSQL
 Source Server Version : 130000
 Source Host           : localhost:5432
 Source Catalog        : Cuti_Imam
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 130000
 File Encoding         : 65001

 Date: 19/11/2020 09:04:54
*/


-- ----------------------------
-- Sequence structure for bucket_approval_id_bucket_approval_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."bucket_approval_id_bucket_approval_seq";
CREATE SEQUENCE "public"."bucket_approval_id_bucket_approval_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for position_id_position_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."position_id_position_seq";
CREATE SEQUENCE "public"."position_id_position_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for position_leave_id_position_leave_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."position_leave_id_position_leave_seq";
CREATE SEQUENCE "public"."position_leave_id_position_leave_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for user_request_leave_id_user_request_leave_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."user_request_leave_id_user_request_leave_seq";
CREATE SEQUENCE "public"."user_request_leave_id_user_request_leave_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for users_id_users_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."users_id_users_seq";
CREATE SEQUENCE "public"."users_id_users_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Table structure for bucket_approval
-- ----------------------------
DROP TABLE IF EXISTS "public"."bucket_approval";
CREATE TABLE "public"."bucket_approval" (
  "bucket_approval_id" int8 NOT NULL,
  "status" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "resolverReason" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "resolvedBy" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "resolvedDate" date NOT NULL,
  "name" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "request_date" date NOT NULL,
  "user_request_leave_id" int8,
  "created_at" timestamp(6),
  "created_by" varchar(255) COLLATE "pg_catalog"."default",
  "updated_at" timestamp(6),
  "updated_by" varchar(255) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of bucket_approval
-- ----------------------------

-- ----------------------------
-- Table structure for position
-- ----------------------------
DROP TABLE IF EXISTS "public"."position";
CREATE TABLE "public"."position" (
  "position_id" int8 NOT NULL,
  "position_name" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "created_at" timestamp(6),
  "created_by" varchar(255) COLLATE "pg_catalog"."default",
  "updated_at" timestamp(6),
  "updated_by" varchar(255) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of position
-- ----------------------------
INSERT INTO "public"."position" VALUES (1, 'Employee', '2020-11-18 08:41:56', 'owner', '2020-11-19 08:42:10', 'owner');
INSERT INTO "public"."position" VALUES (2, 'Supervisor', '2020-11-18 08:42:26', 'owner', '2020-11-19 08:42:33', 'owner');
INSERT INTO "public"."position" VALUES (3, 'Staff', '2020-11-18 08:42:43', 'owner', '2020-11-19 08:42:48', 'owner');

-- ----------------------------
-- Table structure for position_leave
-- ----------------------------
DROP TABLE IF EXISTS "public"."position_leave";
CREATE TABLE "public"."position_leave" (
  "position_leave_id" int8 NOT NULL,
  "tb_position_leave" date NOT NULL,
  "position_id" int8,
  "leave_total" int4 NOT NULL,
  "created_at" timestamp(6),
  "created_by" varchar(255) COLLATE "pg_catalog"."default",
  "updated_at" timestamp(6),
  "updated_by" varchar(255) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of position_leave
-- ----------------------------
INSERT INTO "public"."position_leave" VALUES (1, '2020-01-01', 1, 12, '2020-11-18 08:43:16', 'owner', '2020-11-19 08:43:21', 'owner');
INSERT INTO "public"."position_leave" VALUES (2, '2020-01-01', 2, 15, '2020-11-18 08:43:43', 'owner', '2020-11-19 08:43:50', 'owner');
INSERT INTO "public"."position_leave" VALUES (3, '2020-01-01', 3, 13, '2020-11-18 08:44:16', 'owner', '2020-11-19 08:44:21', 'owner');

-- ----------------------------
-- Table structure for user_request_leave
-- ----------------------------
DROP TABLE IF EXISTS "public"."user_request_leave";
CREATE TABLE "public"."user_request_leave" (
  "user_request_leave_id" int8 NOT NULL,
  "user_id" int8 NOT NULL,
  "leave_date_from" date NOT NULL,
  "leave_date_to" date NOT NULL,
  "description" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "request_date" date NOT NULL,
  "status" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "resolverReason" varchar(255) COLLATE "pg_catalog"."default",
  "resolvedBy" varchar(255) COLLATE "pg_catalog"."default",
  "resolvedDate" date,
  "leave_total_left" int4 NOT NULL,
  "created_at" timestamp(6),
  "created_by" varchar(255) COLLATE "pg_catalog"."default",
  "updated_at" timestamp(6),
  "updated_by" varchar(255) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of user_request_leave
-- ----------------------------

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS "public"."users";
CREATE TABLE "public"."users" (
  "user_id" int8 NOT NULL,
  "name" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "leave_total" int4 NOT NULL,
  "position_id" int8 NOT NULL,
  "created_at" timestamp(6),
  "created_by" varchar(255) COLLATE "pg_catalog"."default",
  "updated_at" timestamp(6),
  "updated_by" varchar(255) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of users
-- ----------------------------

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."bucket_approval_id_bucket_approval_seq"', 2, false);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."position_id_position_seq"', 5, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."position_leave_id_position_leave_seq"', 4, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."user_request_leave_id_user_request_leave_seq"', 2, false);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."users_id_users_seq"', 5, true);

-- ----------------------------
-- Primary Key structure for table bucket_approval
-- ----------------------------
ALTER TABLE "public"."bucket_approval" ADD CONSTRAINT "bucket_approval_pkey" PRIMARY KEY ("bucket_approval_id");

-- ----------------------------
-- Primary Key structure for table position
-- ----------------------------
ALTER TABLE "public"."position" ADD CONSTRAINT "position_pkey" PRIMARY KEY ("position_id");

-- ----------------------------
-- Primary Key structure for table position_leave
-- ----------------------------
ALTER TABLE "public"."position_leave" ADD CONSTRAINT "position_leave_pkey" PRIMARY KEY ("position_leave_id");

-- ----------------------------
-- Primary Key structure for table user_request_leave
-- ----------------------------
ALTER TABLE "public"."user_request_leave" ADD CONSTRAINT "user_request_leave_pkey" PRIMARY KEY ("user_request_leave_id");

-- ----------------------------
-- Primary Key structure for table users
-- ----------------------------
ALTER TABLE "public"."users" ADD CONSTRAINT "users_pkey" PRIMARY KEY ("user_id");

-- ----------------------------
-- Foreign Keys structure for table bucket_approval
-- ----------------------------
ALTER TABLE "public"."bucket_approval" ADD CONSTRAINT "user_request_leave_id" FOREIGN KEY ("user_request_leave_id") REFERENCES "public"."user_request_leave" ("user_request_leave_id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table position_leave
-- ----------------------------
ALTER TABLE "public"."position_leave" ADD CONSTRAINT "position_and_position_leave" FOREIGN KEY ("position_id") REFERENCES "public"."position" ("position_id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table user_request_leave
-- ----------------------------
ALTER TABLE "public"."user_request_leave" ADD CONSTRAINT "user_id" FOREIGN KEY ("user_id") REFERENCES "public"."users" ("user_id") ON DELETE CASCADE ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table users
-- ----------------------------
ALTER TABLE "public"."users" ADD CONSTRAINT "position_id" FOREIGN KEY ("position_id") REFERENCES "public"."position" ("position_id") ON DELETE CASCADE ON UPDATE NO ACTION;
