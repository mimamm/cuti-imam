package com.example.CutiImam.exceptions;

public class UserLeaveRequestException extends RuntimeException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6543034984085929259L;

	private String resourceName;
    private String fieldName;
    private Object fieldValue;

    public UserLeaveRequestException(String message) {
    	super(message);
    }

    public UserLeaveRequestException( String resourceName, String fieldName) {
    	super(String.format("%s tidak bisa melakukan approval kepada %s", resourceName, fieldName));
    	this.resourceName = resourceName;
    	this.fieldName = fieldName;
    }

    public UserLeaveRequestException( String resourceName, String fieldName, Object fieldValue) {
        super(String.format("%s dengan %s %s tidak ditemukan", resourceName, fieldName, fieldValue));
        this.resourceName = resourceName;
        this.fieldName = fieldName;
        this.fieldValue = fieldValue;
    }

    public String getResourceName() {
        return resourceName;
    }

    public String getFieldName() {
        return fieldName;
    }

    public Object getFieldValue() {
        return fieldValue;
    }
    
}