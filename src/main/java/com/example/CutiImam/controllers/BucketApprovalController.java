package com.example.CutiImam.controllers;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.CutiImam.dtos.BucketApprovalDto;
import com.example.CutiImam.exceptions.ResourceNotFoundException;
import com.example.CutiImam.exceptions.UserLeaveRequestException;
import com.example.CutiImam.models.BucketApproval;
import com.example.CutiImam.models.UserRequestLeave;
import com.example.CutiImam.models.Users;
import com.example.CutiImam.repositories.BucketApprovalRepository;
import com.example.CutiImam.repositories.UserRequestLeaveRepository;
import com.example.CutiImam.repositories.UsersRepository;

@RestController
@RequestMapping("/api")
public class BucketApprovalController {
	
	@Autowired
	BucketApprovalRepository bucketApprovalRepository;
	
	@Autowired
	UserRequestLeaveRepository userRequestLeaveRepository;
	
	@Autowired
	UsersRepository usersRepository;
	
	ModelMapper modelMapper = new ModelMapper();
	
//	API Resolve Request Leave (No.4)
	@PostMapping("/resolveRequestLeave")
	public Map<String, Object> resolveRequestLeave(@RequestBody BucketApprovalDto body) {
		Map<String, Object> output = new HashMap<String, Object>();
		
		BucketApproval bucketApprovalEntity = modelMapper.map(body, BucketApproval.class);
		UserRequestLeave userRequestLeaveEntity = validasiId(bucketApprovalEntity.getUserRequestLeave().getUserRequestLeaveId());
		
		validasiApprover(userRequestLeaveEntity, getUserDataById(bucketApprovalEntity.getResolvedBy().getUserId()));

		bucketApprovalEntity.setRequestDate(userRequestLeaveEntity.getRequestDate());
		
		validasiTanggal(bucketApprovalEntity.getRequestDate(), bucketApprovalEntity.getResolvedDate());
		validasiKeputusan(bucketApprovalEntity.getStatus(), userRequestLeaveEntity.getUsers(), userRequestLeaveEntity.getLeaveDateFrom(), userRequestLeaveEntity.getLeaveDateTo());
		
		bucketApprovalEntity.setName(userRequestLeaveEntity.getUsers().getName());
		
		userRequestLeaveEntity.setStatus(bucketApprovalEntity.getStatus());
		userRequestLeaveEntity.setResolvedBy(bucketApprovalEntity.getResolvedBy());
		userRequestLeaveEntity.setResolvedDate(bucketApprovalEntity.getResolvedDate());
		userRequestLeaveEntity.setResolverReason(bucketApprovalEntity.getResolverReason());
		
		bucketApprovalRepository.save(bucketApprovalEntity);
		userRequestLeaveRepository.save(userRequestLeaveEntity);
		
		output.put("message", "Permohonan dengan ID " + userRequestLeaveEntity.getUserRequestLeaveId() + " telah berhasil diputuskan");
		
		return output;
	}
	
//	===================================================================================================================================
	
//	Validasi ID
	public UserRequestLeave validasiId(Long id) {
		UserRequestLeave userRequestLeaveEntity =
				userRequestLeaveRepository.findById(id).orElseThrow(() -> new UserLeaveRequestException("Permohonan", "ID", id));
		
		return userRequestLeaveEntity;
	}
	
//	Validasi Tanggal
	public void validasiTanggal(Date tanggalRequest, Date tanggalResolve) {
		if (tanggalResolve.before(tanggalRequest)) {
			throw new UserLeaveRequestException("Kesalahan data, tanggal keputusan tidak bisa lebih awal dari pengajuan cuti.");
		}
	}
	
//	Validasi Approver
	public void validasiApprover(UserRequestLeave userRequestLeave, Users user) {
		if (userRequestLeave.getUsers().getPosition().getPositionName().equalsIgnoreCase("Employee")) {
			if (!user.getPosition().getPositionName().equalsIgnoreCase("Supervisor")) {
				throw new UserLeaveRequestException(user.getPosition().getPositionName(), "Employee");
			}
		} else if (userRequestLeave.getUsers().getPosition().getPositionName().equalsIgnoreCase("Supervisor")) {
			if (!user.getPosition().getPositionName().equalsIgnoreCase("Supervisor")) {
				throw new UserLeaveRequestException(user.getPosition().getPositionName(), "Supervisor");
			} else if (userRequestLeave.getUsers().getUserId() == user.getUserId()) {
				throw new UserLeaveRequestException("Supervisor tidak bisa di Resolve oleh dirinya sendiri.");
			}
		} else if (userRequestLeave.getUsers().getPosition().getPositionName().equalsIgnoreCase("Staff")) {
			if (!user.getPosition().getPositionName().equalsIgnoreCase("Staff")) {
				throw new UserLeaveRequestException(user.getPosition().getPositionName(), "Staff");
			}
		}
	}
	
//	Validasi Keputusan
	public void validasiKeputusan(String status, Users user, Date dateFrom, Date dateTo) {
		if (status.equalsIgnoreCase("approved")) {
			user.setLeaveTotal(calculateLeaveTotal(user, dateFrom, dateTo));
			
			usersRepository.save(user);
		}
	}
	
//	=============================================================================================================================

//	Calculate Jatah Cuti
	public int calculateLeaveTotal(Users user, Date dateFrom, Date dateTo) {
		long diff = dateTo.getTime() - dateFrom.getTime();
		long daysBetween = (diff / (24 * 60 * 60 * 1000)) + 1;
		
		int result = (int) (user.getLeaveTotal() - daysBetween);
		return result;
	}
	
//	=============================================================================================================================
	
//	Get User data by Id
	public Users getUserDataById(Long id) {
		Users user = usersRepository.findById(id).orElseThrow(()-> new ResourceNotFoundException("User", "ID", id));
		
		return user;
	}

}