package com.example.CutiImam.dtos;

import java.util.Date;

public class UserRequestLeaveDto {
	private long userRequestLeaveId;
	private UsersDto users;
	private Date leaveDateFrom;
	private Date leaveDateTo;
	private String description;
	private Date requestDate;
	private String status;
	private String resolverReason;
	private UsersDto resolvedBy;
	private Date resolvedDate;
	private int leaveTotalLeft;
	private Date createdAt;
	private String createdBy;
	private Date updatedAt;
	private String updatedBy;
	
	public UserRequestLeaveDto() {
		// TODO Auto-generated constructor stub
	}

	public UserRequestLeaveDto(long userRequestLeaveId, UsersDto users, Date leaveDateFrom, Date leaveDateTo,
			String description, Date requestDate, String status, String resolverReason, UsersDto resolvedBy,
			Date resolvedDate, int leaveTotalLeft, Date createdAt, String createdBy, Date updatedAt, String updatedBy) {
		super();
		this.userRequestLeaveId = userRequestLeaveId;
		this.users = users;
		this.leaveDateFrom = leaveDateFrom;
		this.leaveDateTo = leaveDateTo;
		this.description = description;
		this.requestDate = requestDate;
		this.status = status;
		this.resolverReason = resolverReason;
		this.resolvedBy = resolvedBy;
		this.resolvedDate = resolvedDate;
		this.leaveTotalLeft = leaveTotalLeft;
		this.createdAt = createdAt;
		this.createdBy = createdBy;
		this.updatedAt = updatedAt;
		this.updatedBy = updatedBy;
	}

	public long getUserRequestLeaveId() {
		return userRequestLeaveId;
	}

	public void setUserRequestLeaveId(long userRequestLeaveId) {
		this.userRequestLeaveId = userRequestLeaveId;
	}

	public UsersDto getUsers() {
		return users;
	}

	public void setUsers(UsersDto users) {
		this.users = users;
	}

	public Date getLeaveDateFrom() {
		return leaveDateFrom;
	}

	public void setLeaveDateFrom(Date leaveDateFrom) {
		this.leaveDateFrom = leaveDateFrom;
	}

	public Date getLeaveDateTo() {
		return leaveDateTo;
	}

	public void setLeaveDateTo(Date leaveDateTo) {
		this.leaveDateTo = leaveDateTo;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getRequestDate() {
		return requestDate;
	}

	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getResolverReason() {
		return resolverReason;
	}

	public void setResolverReason(String resolverReason) {
		this.resolverReason = resolverReason;
	}

	public UsersDto getResolvedBy() {
		return resolvedBy;
	}

	public void setResolvedBy(UsersDto resolvedBy) {
		this.resolvedBy = resolvedBy;
	}

	public Date getResolvedDate() {
		return resolvedDate;
	}

	public void setResolvedDate(Date resolvedDate) {
		this.resolvedDate = resolvedDate;
	}

	public int getLeaveTotalLeft() {
		return leaveTotalLeft;
	}

	public void setLeaveTotalLeft(int leaveTotalLeft) {
		this.leaveTotalLeft = leaveTotalLeft;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	
}