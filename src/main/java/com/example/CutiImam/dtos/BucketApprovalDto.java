package com.example.CutiImam.dtos;

import java.util.Date;

public class BucketApprovalDto {
	private long bucketApprovalId;
	private UserRequestLeaveDto userRequestLeave;
	private String status;
	private String resolverReason;
	private UsersDto resolvedBy;
	private Date resolvedDate;
	private String name;
	private Date requestDate;
	private Date createdAt;
	private String createdBy;
	private Date updatedAt;
	private String updatedBy;
	
	public BucketApprovalDto() {
		// TODO Auto-generated constructor stub
	}

	public BucketApprovalDto(long bucketApprovalId, UserRequestLeaveDto userRequestLeave, String status,
			String resolverReason, UsersDto resolvedBy, Date resolvedDate, String name, Date requestDate, Date createdAt,
			String createdBy, Date updatedAt, String updatedBy) {
		super();
		this.bucketApprovalId = bucketApprovalId;
		this.userRequestLeave = userRequestLeave;
		this.status = status;
		this.resolverReason = resolverReason;
		this.resolvedBy = resolvedBy;
		this.resolvedDate = resolvedDate;
		this.name = name;
		this.requestDate = requestDate;
		this.createdAt = createdAt;
		this.createdBy = createdBy;
		this.updatedAt = updatedAt;
		this.updatedBy = updatedBy;
	}

	public long getBucketApprovalId() {
		return bucketApprovalId;
	}

	public void setBucketApprovalId(long bucketApprovalId) {
		this.bucketApprovalId = bucketApprovalId;
	}

	public UserRequestLeaveDto getUserRequestLeave() {
		return userRequestLeave;
	}

	public void setUserRequestLeave(UserRequestLeaveDto userRequestLeave) {
		this.userRequestLeave = userRequestLeave;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getResolverReason() {
		return resolverReason;
	}

	public void setResolverReason(String resolverReason) {
		this.resolverReason = resolverReason;
	}

	public UsersDto getResolvedBy() {
		return resolvedBy;
	}

	public void setResolvedBy(UsersDto resolvedBy) {
		this.resolvedBy = resolvedBy;
	}

	public Date getResolvedDate() {
		return resolvedDate;
	}

	public void setResolvedDate(Date resolvedDate) {
		this.resolvedDate = resolvedDate;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getRequestDate() {
		return requestDate;
	}

	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	
}