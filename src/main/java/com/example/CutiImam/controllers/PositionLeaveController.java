package com.example.CutiImam.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.CutiImam.dtos.PositionLeaveDto;
import com.example.CutiImam.exceptions.ResourceNotFoundException;
import com.example.CutiImam.models.PositionLeave;
import com.example.CutiImam.repositories.PositionLeaveRepository;

@RestController
@RequestMapping("/api/positionLeave")
public class PositionLeaveController {

	@Autowired
	PositionLeaveRepository positionLeaveRepository;
	
	ModelMapper modelMapper = new ModelMapper();

//  Create a new Position Leave
    @PostMapping("/create")
    public Map<String, Object> createPositionLeave(@RequestBody PositionLeaveDto body) {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	PositionLeave positionLeaveEntity = modelMapper.map(body, PositionLeave.class);
    	
    	positionLeaveRepository.save(positionLeaveEntity);
    	
    	body.setPositionLeaveId(positionLeaveEntity.getPositionLeaveId());
    	result.put("Message", "Create PositionLeave success");
    	result.put("Result", body);
    	
    	return result;
    }

//  Update a Position Leave
    @PutMapping("/update")
    public Map<String, Object> updatePositionLeave(@RequestParam("positionLeaveId") Long id, @RequestBody PositionLeaveDto body) {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	PositionLeave positionLeaveEntity = positionLeaveRepository.findById(id).orElseThrow(()-> new ResourceNotFoundException("Position Leave", "ID", id));
    	Date createdAt = positionLeaveEntity.getCreatedAt();
    	String createdBy = positionLeaveEntity.getCreatedBy();
    	
    	positionLeaveEntity = modelMapper.map(body, PositionLeave.class);
    	positionLeaveEntity.setPositionLeaveId(id);
    	positionLeaveEntity.setCreatedAt(createdAt);
    	positionLeaveEntity.setCreatedBy(createdBy);
    	
    	positionLeaveRepository.save(positionLeaveEntity);
    	
    	body.setPositionLeaveId(positionLeaveEntity.getPositionLeaveId());;
    	result.put("Message", "Update PositionLeave success");
    	result.put("Result", body);
    	
    	return result;
    }

//  Delete a Position Leave
    @DeleteMapping("/delete")
    public Map<String, Object> deletePositionLeave(@RequestParam("positionLeaveId") Long id) {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	PositionLeave positionLeaveEntity = positionLeaveRepository.findById(id).orElseThrow(()-> new ResourceNotFoundException("Position Leave", "ID", id));
    	
    	PositionLeaveDto positionLeaveDto = modelMapper.map(positionLeaveEntity, PositionLeaveDto.class);
    	
    	positionLeaveRepository.delete(positionLeaveEntity);
    	
    	result.put("Message", "Delete PositionLeave success");
    	result.put("Deleted Data", positionLeaveDto);
    	
    	return result;
    }

//  Get a single Position Leave by ID
    @GetMapping("/get")
    public Map<String, Object> getPositionLeaveById(@RequestParam("positionLeaveId") Long id) {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	PositionLeave positionLeaveEntity = positionLeaveRepository.findById(id).orElseThrow(()-> new ResourceNotFoundException("Position Leave", "ID", id));
    	
    	PositionLeaveDto positionLeaveDto = modelMapper.map(positionLeaveEntity, PositionLeaveDto.class);
    	
    	result.put("Result", positionLeaveDto);
    	
    	return result;
    }
    
//  Get all Position Leave
    @GetMapping("/getAll")
    public Map<String, Object> getAllPositionLeave() {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	List<PositionLeave> listEntity = positionLeaveRepository.findAll();
    	List<PositionLeaveDto> listDto = new ArrayList<PositionLeaveDto>();

    	for (PositionLeave positionLeaveEntity : listEntity) {
    		PositionLeaveDto positionLeaveDto = modelMapper.map(positionLeaveEntity, PositionLeaveDto.class);
    		
    		listDto.add(positionLeaveDto);
    	}
    	
    	result.put("Result", listDto);
    	
    	return result;
    }
}