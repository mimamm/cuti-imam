package com.example.CutiImam.dtos;

import java.util.Date;

public class PositionLeaveDto {
	private long positionLeaveId;
	private PositionDto position;
	private Date tbPositionLeave;
	private int leaveTotal;
	private Date createdAt;
	private String createdBy;
	private Date updatedAt;
	private String updatedBy;
	
	public PositionLeaveDto() {
		// TODO Auto-generated constructor stub
	}

	public PositionLeaveDto(long positionLeaveId, PositionDto position, Date tbPositionLeave, int leaveTotal,
			Date createdAt, String createdBy, Date updatedAt, String updatedBy) {
		super();
		this.positionLeaveId = positionLeaveId;
		this.position = position;
		this.tbPositionLeave = tbPositionLeave;
		this.leaveTotal = leaveTotal;
		this.createdAt = createdAt;
		this.createdBy = createdBy;
		this.updatedAt = updatedAt;
		this.updatedBy = updatedBy;
	}

	public long getPositionLeaveId() {
		return positionLeaveId;
	}

	public void setPositionLeaveId(long positionLeaveId) {
		this.positionLeaveId = positionLeaveId;
	}

	public PositionDto getPosition() {
		return position;
	}

	public void setPosition(PositionDto position) {
		this.position = position;
	}

	public Date getTbPositionLeave() {
		return tbPositionLeave;
	}

	public void setTbPositionLeave(Date tbPositionLeave) {
		this.tbPositionLeave = tbPositionLeave;
	}

	public int getLeaveTotal() {
		return leaveTotal;
	}

	public void setLeaveTotal(int leaveTotal) {
		this.leaveTotal = leaveTotal;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	
}